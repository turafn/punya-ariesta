            <div class="col-xs-12">  
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Konfirmasi Pembayaran Konsumen</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr >
                        <th scope="col">NO</th>
                        <th scope="col">Kode Transaksi</th>
                        <th scope="col">Total Transfer</th>
                        <th scope="col">Nama Pengirim</th>
                        <th scope="col">Tanggal Transfer</th>
                        <th scope="col">Bukti Transfer</th>
                        <th scope="col">Waktu Konfirmasi</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                  <?php
                  $no = 1;
                    foreach ($record->result_array() as $r ) { 
                    echo "<tr>
                            <td>$no</td>
                            <td>$r[kode_transaksi]</td>
                            <td>$r[total_transfer]</td>
                            <td>$r[nama_pengirim]</td>
                            <td>$r[tanggal_transfer]</td>
                            <td><a href='".base_url()."administrator/download_bukti/$r[bukti_transfer]'>Download File</a></td>
                            <td>$r[waktu_konfirmasi]</td>
                            <td><a class='btn btn-warning btn-xs' title='Konfirmasi Data' href='".base_url()."administrator/konfirmasi/$r[id_penjualan]'><i class='fas fa-star'></i></a>
                                <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url()."administrator/delete_penjualan/$r[id_penjualan]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                              </center></td>
                        </tr>";
                    $no++;
                      } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">         